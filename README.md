# Rust Playground - Actors and Redis
This is a small project just to understand how to play with actors in Rust.

Basically the ideia is:
- run the docker-compose with a redis instance
- execute the redis-cli inside the container
- post messages on `lteste` key using LPOP
    - ex: 
         - `LPOP "lteste" "anything" `
         - `LPUSH "lteste" '{"type": "SUM", "data": [1, 2, 3, 4]}'`
         - `LPUSH "lteste" '{"type": "DIFF", "data": [1, 2, 30, 8]}'`


- The system will react to these messages:
    - "TestMessage" => TestActor will be activated
    - "Generic" => GenericActor will respond
    - Anything else => Parser will call a parser 


System Log:
```
Actor Redis Service created
Started parser actor
Started TestMessage actor
Started the greyish actor ever: GenericActor
Sending via generic
Started SUM actor
Started Difference actor

Nobody knows me... Here's my generic message for you: GenericMessage { msg: "Generic" }
Lets parse this message with TESTMESSAGE ACTOR: TestMessage { num: 42 }
Lets parse this message: RedisMessage { content: "msg4" }

```


**Project is under development**

## Changelog
### 2024-02-02
Now it can act as a basic calculator
- Added 2 more actors: Sum and Difference

## Dependencies
- ractor
- redis-rs
- tokio 
- serde_json


## Running
`cargo run`
