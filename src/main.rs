use std::error::Error;

mod actors;
mod messages;

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    //ractor::Actor::spawn(None, service::Service::new("redis://127.0.0.1".to_string(), "lteste".to_string()), ()).await.expect("Error creating Serivce actor");
    actors::ActorSystem::init().await;

    tokio::signal::ctrl_c().await.unwrap();
    Ok(())
}

