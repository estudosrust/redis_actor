mod parser;
mod teste;
mod generic;
mod redis_service;
mod sum;
mod difference;

pub struct ActorSystem;

impl ActorSystem {
    pub async fn init() {

        ractor::Actor::spawn(None, redis_service::RedisService::new("redis://127.0.0.1".to_string(), "lteste".to_string()), ()).await.expect("Error creating Serivce actor");

        ractor::Actor::spawn(Some("Parser".to_string()), parser::Parser, ())
            .await
            .expect("Error creating Parser Actor");

        ractor::Actor::spawn(Some("TestParser".to_string()), teste::TestParser, ())
            .await
            .expect("Error creating Parser Actor");        

        ractor::Actor::spawn(None, generic::GenericActor, ())
            .await
            .expect("Error creating Parser Actor");        

        ractor::Actor::spawn(Some("SumCalculator".to_string()), sum::SumCalculator, ())
            .await
            .expect("Error creating SumCalculator Actor");        

        ractor::Actor::spawn(Some("DifferenceCalculator".to_string()), difference::DifferenceCalculator, ())
            .await
            .expect("Error creating SumCalculator Actor");        
    }
}
