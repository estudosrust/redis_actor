use ractor::{Actor, ActorProcessingErr, ActorRef, async_trait};

use crate::messages::GenericMessage;

pub struct GenericActor;

#[async_trait]
impl Actor for GenericActor {
    type Msg = GenericMessage;
    type State = ();
    type Arguments = ();

    async fn pre_start(&self, myself: ActorRef<Self::Msg>, _: ()) -> Result<Self::State, ActorProcessingErr> {
        println!("Started the greyish actor ever: GenericActor");
        ractor::pg::join("actors".to_string(), vec![myself.get_cell()]);

        Ok(())
    } 

    async fn handle(&self, _myself: ActorRef<Self::Msg>, msg: Self::Msg, _state: &mut Self::State) -> Result<(), ActorProcessingErr> {
        println!("Nobody knows me... Here's my generic message for you: {:?}", msg);

        Ok(())
    }


}
