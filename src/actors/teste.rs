use ractor::{Actor, ActorProcessingErr, ActorRef, async_trait};

use crate::messages::TestMessage;

pub struct TestParser;

#[async_trait]
impl Actor for TestParser {
    type Msg = TestMessage;
    type State = ();
    type Arguments = ();

    async fn pre_start(&self, myself: ActorRef<Self::Msg>, _: ()) -> Result<Self::State, ActorProcessingErr> {
        println!("Started TestMessage actor");
        ractor::pg::join("actors".to_string(), vec![myself.get_cell()]);

        Ok(())
    } 

    async fn handle(&self, _myself: ActorRef<Self::Msg>, msg: Self::Msg, _state: &mut Self::State) -> Result<(), ActorProcessingErr> {
        println!("Lets parse this message with TESTMESSAGE ACTOR: {:?}", msg);

        Ok(())
    }


}
