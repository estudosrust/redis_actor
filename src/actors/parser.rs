use ractor::{Actor, ActorProcessingErr, ActorRef, async_trait};

use crate::messages::{
    SystemMessage, 
    redismessage::RedisMessage, 
    operations::{Sum, Difference}
};
use serde_json::Value;

pub struct Parser;

#[async_trait]
impl Actor for Parser {
    type Msg = RedisMessage;
    type State = ();
    type Arguments = ();

    async fn pre_start(&self, myself: ActorRef<Self::Msg>, _: ()) -> Result<Self::State, ActorProcessingErr> {
        println!("Started parser actor");
        ractor::pg::join("actors".to_string(), vec![myself.get_cell()]);

        Ok(())
    } 

    async fn handle(&self, _myself: ActorRef<Self::Msg>, msg: Self::Msg, _state: &mut Self::State) -> Result<(), ActorProcessingErr> {
        let parsed = serde_json::from_str(&msg.content);

        match parsed {
            Ok(v) => self.validate(v),
            Err(_e) => println!("Not a valid JSON structure")
        }

        Ok(())
    }
}

impl Parser {
    fn validate(&self, parsed: Value) {
        match parsed["type"].as_str() {
            Some("SUM") => {
                let val: Vec<i32> = self.get_vec_i32(parsed["data"].clone());
                Sum { val }.send();    
            },
            Some("DIFF") => {
                let val: Vec<i32> = self.get_vec_i32(parsed["data"].clone());
                Difference { val }.send();    
            },
            _ => println!("Dont know what to do")
        }
    }


    fn get_vec_i32(&self, val: Value) -> Vec<i32> {
        val.as_array().unwrap()
            .into_iter().map(|v| serde_json::from_value(v.clone()).unwrap())
            .collect()
    }
}    
