use ractor::{Actor, ActorProcessingErr, ActorRef, async_trait};

use crate::messages::operations::Difference;

pub struct DifferenceCalculator;

#[async_trait]
impl Actor for DifferenceCalculator {
    type Msg = Difference;
    type State = ();
    type Arguments = ();

    async fn pre_start(&self, myself: ActorRef<Self::Msg>, _: ()) -> Result<Self::State, ActorProcessingErr> {
        println!("Started Difference actor");
        ractor::pg::join("actors".to_string(), vec![myself.get_cell()]);

        Ok(())
    } 

    async fn handle(&self, _myself: ActorRef<Self::Msg>, msg: Self::Msg, _state: &mut Self::State) -> Result<(), ActorProcessingErr> {
        let res = msg.val
            .iter()
            .fold(0, |x, y| x - y);

        println!("Difference of values {:?} = {}", msg.val, res);

        Ok(())
    }


}
