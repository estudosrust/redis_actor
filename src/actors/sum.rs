use ractor::{Actor, ActorProcessingErr, ActorRef, async_trait};

use crate::messages::operations::Sum;

pub struct SumCalculator;

#[async_trait]
impl Actor for SumCalculator {
    type Msg = Sum;
    type State = ();
    type Arguments = ();

    async fn pre_start(&self, myself: ActorRef<Self::Msg>, _: ()) -> Result<Self::State, ActorProcessingErr> {
        println!("Started SUM actor");
        ractor::pg::join("actors".to_string(), vec![myself.get_cell()]);

        Ok(())
    } 

    async fn handle(&self, _myself: ActorRef<Self::Msg>, msg: Self::Msg, _state: &mut Self::State) -> Result<(), ActorProcessingErr> {
        let res = msg.val
            .iter()
            .fold(0, |x, y| x + y);

        println!("Sum of values {:?} = {}", msg.val, res);

        Ok(())
    }


}
