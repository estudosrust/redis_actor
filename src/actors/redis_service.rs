use ractor::{async_trait, Actor, ActorRef, ActorProcessingErr};

use crate::messages::{TestMessage, SystemMessage, redismessage::RedisMessage, GenericMessage};

#[derive(Debug, Clone)]
pub enum ServiceStatus {
    ACTIVE
}

pub struct RedisService {
   uri: String,
   channel: String
}

impl RedisService {
  pub fn new(uri: String, channel: String) -> Self {
    RedisService {
       uri,
       channel
    }
  }
}

#[async_trait]
impl Actor for RedisService {
    type Msg = ServiceStatus;
    type State = redis::Connection;
    type Arguments = ();

    async fn pre_start(&self, myself: ActorRef<Self::Msg>, _: ()) -> Result<Self::State, ActorProcessingErr> {
        println!("Actor Redis Service created");

        let client = redis::Client::open(self.uri.as_str()).unwrap();
        let con = client.get_connection().unwrap();

        myself.send_message(ServiceStatus::ACTIVE).unwrap();
        Ok(con)
    }

    async fn handle(&self, myself: ActorRef<Self::Msg>, msg: Self::Msg, state: &mut Self::State) -> Result<(), ActorProcessingErr> {
        match msg {
            ServiceStatus::ACTIVE => {
                if let Ok(m) = redis::cmd("LPOP").arg(self.channel.as_str()).query::<String>(state) {
                    match m.as_str() {
                        "TestMessage" => TestMessage { num: 42 }.send(),
                        "Generic" => GenericMessage { msg:  m }.send(),
                        _ => RedisMessage { content: m }.send()
                    }

                } else {}

                myself.send_message(ServiceStatus::ACTIVE).unwrap();
           }
        }
        Ok(())
    }

}

