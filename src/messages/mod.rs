use ractor::ActorRef;

pub mod redismessage; 
pub mod operations;

pub trait SystemMessage {
    fn send(&self);
}

#[derive(Clone, Debug)]
pub struct TestMessage {
   pub num: u8
}


impl SystemMessage for TestMessage {
    fn send(&self) {
        if let Some(act) = ractor::ActorRef::<Self>::where_is("TestParser".to_string()) {
            let _ = act.cast(self.clone());
        }
    }
}



#[derive(Clone, Debug)]
pub struct GenericMessage {
   pub msg: String
}


impl SystemMessage for GenericMessage {
    fn send(&self) {
        println!("Sending via generic");
        let actors = ractor::pg::get_members(&"actors".to_string())
                        .into_iter()
                        .map(ActorRef::<Self>::from)
                        .collect::<Vec<_>>();

        for act in actors {
            let _ = act.cast(self.clone());
        }

    }
}
