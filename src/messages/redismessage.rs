use super::SystemMessage;

#[derive(Debug, Clone)]
pub struct RedisMessage {
    pub content: String
}

impl SystemMessage for RedisMessage {
    fn send(&self) {
        if let Some(act) = ractor::ActorRef::<Self>::where_is("Parser".to_string()) {
            let _ = act.cast(self.clone());
        }
    }
}
