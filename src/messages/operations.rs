use super::SystemMessage;

#[derive(Debug, Clone)]
pub struct Sum {
    pub val: Vec<i32>
}

impl SystemMessage for Sum {
    fn send(&self) {
        if let Some(act) = ractor::ActorRef::<Self>::where_is("SumCalculator".to_string()) {
            let _ = act.cast(self.clone());
        }
    }
}

#[derive(Debug, Clone)]
pub struct Difference {
    pub val: Vec<i32>
}

impl SystemMessage for Difference {
    fn send(&self) {
        if let Some(act) = ractor::ActorRef::<Self>::where_is("DifferenceCalculator".to_string()) {
            let _ = act.cast(self.clone());
        }
    }
}
